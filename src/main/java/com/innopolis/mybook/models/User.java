package com.innopolis.mybook.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private String nickName;
    private String email;
    private String aboutMyself;

    @OneToMany(mappedBy = "owner")
    private List<Review> reviews;

    @OneToMany(mappedBy = "owner")
    private List<Book> books;
}
