package com.innopolis.mybook.forms;

import lombok.Data;

@Data
public class BooksForm {

    private String name;
    private Integer year;
    private String author;
    private String genre;
    private String link;
}
