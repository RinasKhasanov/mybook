package com.innopolis.mybook.forms;

import lombok.Data;

@Data
public class ReviewsForm {

    private String book;
    private String text;
    private Integer estimate;
}
