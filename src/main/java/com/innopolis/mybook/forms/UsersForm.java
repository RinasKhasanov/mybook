package com.innopolis.mybook.forms;

import lombok.Data;

@Data
public class UsersForm {

    private String firstName;
    private String lastName;
    private String nickName;
    private String email;
    private String aboutMyself;
}
