package com.innopolis.mybook.controller;

import com.innopolis.mybook.forms.UsersForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.models.User;
import com.innopolis.mybook.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class UsersController {

    private final UsersService usersService;

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "users_tm/users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = usersService.getUser(userId);
        model.addAttribute("user", user);
        return "users_tm/user_page";
    }

    @PostMapping("/users")
    public String addUser(UsersForm form) {
        usersService.addUser(form);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
        usersService.deleteUser(userId);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Integer userId, UsersForm usersForm) {
        usersService.update(userId, usersForm);
        return "redirect:/users";
    }

    @GetMapping("/users/{user-id}/reviews")
    public String getReviewsByUser(Model model, @PathVariable("user-id") Integer userId) {
        List<Review> reviews = usersService.getReviewsByUser(userId);
        List<Review> unusedReviews = usersService.getReviewsWithoutOwner();
        model.addAttribute("userId", userId);
        model.addAttribute("reviews", reviews);
        model.addAttribute("unusedReviews", unusedReviews);
        return "users_tm/reviews_of_user";
    }

    @PostMapping("/users/{user-id}/reviews")
    public String addReviewToUser(@PathVariable("user-id") Integer userId, @RequestParam("reviewId") Integer reviewId) {
        usersService.addReviewToUser(userId, reviewId);
        return "redirect:/users/" + userId + "/reviews";
    }

    @GetMapping("/users/{user-id}/books")
    public String getBooksByUser(Model model, @PathVariable("user-id") Integer userId) {
        List<Book> books = usersService.getBooksByUser(userId);
        List<Book> unusedBooks = usersService.getBooksWithoutOwner();
        model.addAttribute("userId", userId);
        model.addAttribute("books", books);
        model.addAttribute("unusedBooks", unusedBooks);
        return "users_tm/books_of_user";
    }

    @PostMapping("/users/{user-id}/books")
    public String addBookToUser(@PathVariable("user-id") Integer userId, @RequestParam("bookId") Integer bookId) {
        usersService.addBookToUser(userId, bookId);
        return "redirect:/users/" + userId + "/books";
    }
}
