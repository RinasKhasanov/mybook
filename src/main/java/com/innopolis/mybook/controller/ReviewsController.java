package com.innopolis.mybook.controller;

import com.innopolis.mybook.forms.ReviewsForm;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.services.ReviewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class ReviewsController {

    private final ReviewsService reviewsService;

    @GetMapping("/reviews")
    public String getReviewsPage(Model model) {
        List<Review> reviews = reviewsService.getAllReviews();
        model.addAttribute("reviews", reviews);
        return "reviews_tm/reviews";
    }

    @GetMapping("/reviews/{review-id}")
    public String getReviewPage(Model model, @PathVariable("review-id") Integer reviewId) {
        Review review = reviewsService.getReview(reviewId);
        model.addAttribute("review", review);
        return "reviews_tm/review_page";
    }

    @PostMapping("/reviews")
    public String addReview(ReviewsForm form) {
        reviewsService.addReview(form);
        return "redirect:/reviews";
    }

    @PostMapping("/reviews/{review-id}/delete")
    public String deleteReview(@PathVariable("review-id") Integer reviewId) {
        reviewsService.deleteReview(reviewId);
        return "redirect:/reviews";
    }

    @PostMapping("/reviews/{review-id}/update")
    public String updateReview(@PathVariable("review-id") Integer reviewId, ReviewsForm reviewsForm) {
        reviewsService.update(reviewId, reviewsForm);
        return "redirect:/reviews";
    }
}
