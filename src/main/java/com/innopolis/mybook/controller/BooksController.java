package com.innopolis.mybook.controller;

import com.innopolis.mybook.forms.BooksForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.services.BooksService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class BooksController {

    private final BooksService booksService;

    @GetMapping("/books")
    public String getBooksPage(Model model) {
        List<Book> books = booksService.getAllBooks();
        model.addAttribute("books", books);
        return "books_tm/books";
    }

    @GetMapping("/books/{book-id}")
    public String getBookPage(Model model, @PathVariable("book-id") Integer bookId) {
        Book book = booksService.getBook(bookId);
        model.addAttribute("book", book);
        return "books_tm/book_page";
    }

    @PostMapping("/books")
    public String addBook(BooksForm form) {
        booksService.addBook(form);
        return "redirect:/books";
    }

    @PostMapping("/books/{book-id}/delete")
    public String deleteBook(@PathVariable("book-id") Integer bookId) {
        booksService.deleteBook(bookId);
        return "redirect:/books";
    }

    @PostMapping("/books/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Integer bookId, BooksForm booksForm) {
        booksService.update(bookId, booksForm);
        return "redirect:/books";
    }
}
