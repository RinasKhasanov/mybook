package com.innopolis.mybook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.innopolis.mybook.models.User;

public interface UsersRepository extends JpaRepository<User, Integer> {
}
