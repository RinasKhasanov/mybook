package com.innopolis.mybook.repositories;

import com.innopolis.mybook.models.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewsRepository extends JpaRepository<Review, Integer> {

    List<Review> findAllByOwner_Id(Integer id);

    List<Review> findAllByOwnerIsNull();
}
