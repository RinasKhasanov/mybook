package com.innopolis.mybook.repositories;

import com.innopolis.mybook.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Integer> {

    List<Book> findAllByOwner_Id(Integer id);

    List<Book> findAllByOwnerIsNull();
}
