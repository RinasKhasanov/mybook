package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.BooksForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.repositories.BooksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class BooksServiceImpl implements BooksService {

    private final BooksRepository booksRepository;

    @Override
    public Book addBook(BooksForm form) {

        Book book = Book.builder()
                .name(form.getName())
                .year(form.getYear())
                .author(form.getAuthor())
                .genre(form.getGenre())
                .link(form.getLink())
                .build();

        booksRepository.save(book);
        return book;
    }

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAll();
    }

    @Override
    public void deleteBook(Integer bookId) {
        booksRepository.deleteById(bookId);
    }

    @Override
    public Book getBook(Integer bookId) {
        return booksRepository.getById(bookId);
    }

    @Override
    public Book update(Integer bookId, BooksForm booksForm) {

        Book book = booksRepository.getById(bookId);
        book.setName(booksForm.getName());
        book.setYear(booksForm.getYear());
        book.setAuthor(booksForm.getAuthor());
        book.setGenre(booksForm.getGenre());
        book.setLink(booksForm.getLink());

        booksRepository.save(book);
        return book;
    }
}
