package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.BooksForm;
import com.innopolis.mybook.models.Book;

import java.util.List;

public interface BooksService {

    Book addBook(BooksForm form);

    List<Book> getAllBooks();

    void deleteBook(Integer bookId);

    Book getBook(Integer bookId);

    Book update(Integer bookId, BooksForm booksForm);
}
