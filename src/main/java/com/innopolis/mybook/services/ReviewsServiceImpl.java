package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.ReviewsForm;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.repositories.ReviewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ReviewsServiceImpl implements ReviewsService {

    private final ReviewsRepository reviewsRepository;

    @Override
    public Review addReview(ReviewsForm form) {

        Review review = Review.builder()
                .book(form.getBook())
                .text(form.getText())
                .estimate(form.getEstimate())
                .build();

        reviewsRepository.save(review);
        return review;
    }

    @Override
    public List<Review> getAllReviews() {
        return reviewsRepository.findAll();
    }

    @Override
    public void deleteReview(Integer reviewId) {
        reviewsRepository.deleteById(reviewId);
    }

    @Override
    public Review getReview(Integer reviewId) {
        return reviewsRepository.getById(reviewId);
    }

    @Override
    public Review update(Integer reviewId, ReviewsForm reviewsForm) {

        Review review = reviewsRepository.getById(reviewId);
        review.setBook(reviewsForm.getBook());
        review.setText(reviewsForm.getText());
        review.setEstimate(reviewsForm.getEstimate());

        reviewsRepository.save(review);
        return review;
    }
}
