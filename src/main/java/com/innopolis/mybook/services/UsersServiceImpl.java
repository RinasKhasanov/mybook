package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.UsersForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.models.User;
import com.innopolis.mybook.repositories.BooksRepository;
import com.innopolis.mybook.repositories.ReviewsRepository;
import com.innopolis.mybook.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final ReviewsRepository reviewsRepository;

    private final BooksRepository booksRepository;

    @Override
    public User addUser(UsersForm form) {

        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .nickName(form.getNickName())
                .email(form.getEmail())
                .aboutMyself(form.getAboutMyself())
                .build();

        usersRepository.save(user);
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.getById(userId);
    }

    @Override
    public User update(Integer userId, UsersForm usersForm) {

        User user = usersRepository.getById(userId);
        user.setFirstName(usersForm.getFirstName());
        user.setLastName(usersForm.getLastName());
        user.setNickName(usersForm.getNickName());
        user.setEmail(usersForm.getEmail());
        user.setAboutMyself(usersForm.getAboutMyself());

        usersRepository.save(user);
        return user;
    }

    @Override
    public List<Review> getReviewsByUser(Integer userId) {
        return reviewsRepository.findAllByOwner_Id(userId);
    }

    @Override
    public List<Review> getReviewsWithoutOwner() {
        return reviewsRepository.findAllByOwnerIsNull();
    }

    @Override
    public void addReviewToUser(Integer userId, Integer reviewId) {

        User user = usersRepository.getById(userId);
        Review review = reviewsRepository.getById(reviewId);
        review.setOwner(user);
        reviewsRepository.save(review);
    }

    @Override
    public List<Book> getBooksByUser(Integer userId) {
        return booksRepository.findAllByOwner_Id(userId);
    }

    @Override
    public List<Book> getBooksWithoutOwner() {
        return booksRepository.findAllByOwnerIsNull();
    }

    @Override
    public void addBookToUser(Integer userId, Integer bookId) {

        User user = usersRepository.getById(userId);
        Book book = booksRepository.getById(bookId);
        book.setOwner(user);
        booksRepository.save(book);
    }
}