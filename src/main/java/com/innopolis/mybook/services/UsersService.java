package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.UsersForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.models.User;

import java.util.List;

public interface UsersService {

    User addUser(UsersForm form);

    List<User> getAllUsers();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    User update(Integer userId, UsersForm usersForm);

    List<Review> getReviewsByUser(Integer userId);

    List<Review> getReviewsWithoutOwner();

    void addReviewToUser(Integer userId, Integer reviewId);

    List<Book> getBooksByUser(Integer userId);

    List<Book> getBooksWithoutOwner();

    void addBookToUser(Integer userId, Integer bookId);
}
