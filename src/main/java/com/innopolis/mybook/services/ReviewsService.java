package com.innopolis.mybook.services;

import com.innopolis.mybook.forms.ReviewsForm;
import com.innopolis.mybook.models.Review;

import java.util.List;

public interface ReviewsService {

    Review addReview(ReviewsForm form);

    List<Review> getAllReviews();

    void deleteReview(Integer reviewId);

    Review getReview(Integer reviewId);

    Review update(Integer reviewId, ReviewsForm reviewsForm);
}
