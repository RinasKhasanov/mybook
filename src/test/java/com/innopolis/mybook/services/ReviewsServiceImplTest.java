package com.innopolis.mybook.services;

import com.innopolis.mybook.TestMybookApplicationTests;
import com.innopolis.mybook.forms.ReviewsForm;
import com.innopolis.mybook.models.Review;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReviewsServiceImplTest extends TestMybookApplicationTests {

    @Autowired
    ReviewsService reviewsServiceImpl;

    private ReviewsForm getReviewFormTest() {

        ReviewsForm reviewFormTest = new ReviewsForm();
        reviewFormTest.setBook("test1");
        reviewFormTest.setText("test2");
        reviewFormTest.setEstimate(1);

        return reviewFormTest;
    }

    @Test
    void getAllReviews() {
        List<Review> reviewListTest = reviewsServiceImpl.getAllReviews();
        assertNotNull(reviewListTest);
    }

    @Test
    @Transactional
    void addReview() {
        Review review = reviewsServiceImpl.addReview(getReviewFormTest());
        assertNotNull(review);
        assertEquals("test1", review.getBook());
        assertEquals("test2", review.getText());
        assertEquals(1, review.getEstimate());
    }

    @Test
    @Transactional
    void deleteReview() {
        Review review = reviewsServiceImpl.addReview(getReviewFormTest());
        List<Review> reviewListTest = reviewsServiceImpl.getAllReviews();
        assertEquals(1, reviewListTest.size());
        Integer id = review.getId();
        reviewsServiceImpl.deleteReview(id);
        reviewListTest = reviewsServiceImpl.getAllReviews();
        assertEquals(0, reviewListTest.size());
    }

    @Test
    @Transactional
    void getReview() {
        Review reviewCreate = reviewsServiceImpl.addReview(getReviewFormTest());
        Review review = reviewsServiceImpl.getReview(reviewCreate.getId());
        assertEquals(review, reviewCreate);
    }

    @Test
    @Transactional
    void update() {
        Review reviewCreate = reviewsServiceImpl.addReview(getReviewFormTest());
        ReviewsForm reviewsForm = getReviewFormTest();
        reviewsForm.setBook("name");
        Review reviewUpdate = reviewsServiceImpl.update(reviewCreate.getId(), reviewsForm);
        assertNotEquals(reviewUpdate.getBook(), "test1");
        assertEquals(reviewCreate.getId(), reviewUpdate.getId());
    }
}