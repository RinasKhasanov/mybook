package com.innopolis.mybook.services;

import com.innopolis.mybook.TestMybookApplicationTests;
import com.innopolis.mybook.forms.BooksForm;
import com.innopolis.mybook.models.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BooksServiceImplTest extends TestMybookApplicationTests {

    @Autowired
    BooksService booksServiceImpl;

    private BooksForm getBookFormTest() {

        BooksForm bookFormTest = new BooksForm();
        bookFormTest.setName("test1");
        bookFormTest.setYear(1);
        bookFormTest.setAuthor("test3");
        bookFormTest.setGenre("test4");
        bookFormTest.setLink("test5");

        return bookFormTest;
    }

    @Test
    void getAllBooks() {
        List<Book> bookListTest = booksServiceImpl.getAllBooks();
        assertNotNull(bookListTest);
    }

    @Test
    @Transactional
    void addBook() {
        Book book = booksServiceImpl.addBook(getBookFormTest());
        assertNotNull(book);
        assertEquals("test1", book.getName());
        assertEquals(1, book.getYear());
        assertEquals("test3", book.getAuthor());
        assertEquals("test4", book.getGenre());
        assertEquals("test5", book.getLink());
    }

    @Test
    @Transactional
    void deleteBook() {
        Book book = booksServiceImpl.addBook(getBookFormTest());
        List<Book> bookListTest = booksServiceImpl.getAllBooks();
        assertEquals(1, bookListTest.size());
        Integer id = book.getId();
        booksServiceImpl.deleteBook(id);
        bookListTest = booksServiceImpl.getAllBooks();
        assertEquals(0, bookListTest.size());
    }

    @Test
    @Transactional
    void getBook() {
        Book bookCreate = booksServiceImpl.addBook(getBookFormTest());
        Book book = booksServiceImpl.getBook(bookCreate.getId());
        assertEquals(book, bookCreate);
    }

    @Test
    @Transactional
    void update() {
        Book bookCreate = booksServiceImpl.addBook(getBookFormTest());
        BooksForm booksForm = getBookFormTest();
        booksForm.setName("name");
        Book bookUpdate = booksServiceImpl.update(bookCreate.getId(), booksForm);
        assertNotEquals(bookUpdate.getName(), "test1");
        assertEquals(bookCreate.getId(), bookUpdate.getId());
    }
}