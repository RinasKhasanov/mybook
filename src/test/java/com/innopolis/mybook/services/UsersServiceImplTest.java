package com.innopolis.mybook.services;

import com.innopolis.mybook.TestMybookApplicationTests;
import com.innopolis.mybook.forms.BooksForm;
import com.innopolis.mybook.forms.ReviewsForm;
import com.innopolis.mybook.forms.UsersForm;
import com.innopolis.mybook.models.Book;
import com.innopolis.mybook.models.Review;
import com.innopolis.mybook.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UsersServiceImplTest extends TestMybookApplicationTests {

    @Autowired
    UsersService usersServiceImpl;

    @Autowired
    ReviewsService reviewsServiceImpl;

    @Autowired
    BooksService booksServiceImpl;

    private BooksForm getBookFormTest() {

        BooksForm bookFormTest = new BooksForm();
        bookFormTest.setName("test1");
        bookFormTest.setYear(1);
        bookFormTest.setAuthor("test3");
        bookFormTest.setGenre("test4");
        bookFormTest.setLink("test5");

        return bookFormTest;
    }

    private UsersForm getUserFormTest() {

        UsersForm userFormTest = new UsersForm();
        userFormTest.setFirstName("test1");
        userFormTest.setLastName("test2");
        userFormTest.setNickName("test3");
        userFormTest.setEmail("test@");
        userFormTest.setAboutMyself("test5");

        return userFormTest;
    }

    private ReviewsForm getReviewFormTest() {

        ReviewsForm reviewFormTest = new ReviewsForm();
        reviewFormTest.setBook("test1");
        reviewFormTest.setText("test2");
        reviewFormTest.setEstimate(2);

        return reviewFormTest;
    }

    @Test
    void getAllUsers() {
        List<User> userListTest = usersServiceImpl.getAllUsers();
        assertNotNull(userListTest);
    }

    @Test
    @Transactional
    void addUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        assertNotNull(user);
        assertEquals("test1", user.getFirstName());
        assertEquals("test2", user.getLastName());
        assertEquals("test3", user.getNickName());
        assertEquals("test@", user.getEmail());
        assertEquals("test5", user.getAboutMyself());
    }

    @Test
    @Transactional
    void deleteUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        List<User> userListTest = usersServiceImpl.getAllUsers();
        assertEquals(1, userListTest.size());
        Integer id = user.getId();
        usersServiceImpl.deleteUser(id);
        userListTest = usersServiceImpl.getAllUsers();
        assertEquals(0, userListTest.size());
    }

    @Test
    @Transactional
    void getUser() {
        User userCreate = usersServiceImpl.addUser(getUserFormTest());
        User user = usersServiceImpl.getUser(userCreate.getId());
        assertEquals(user, userCreate);
    }

    @Test
    @Transactional
    void update() {
        User userCreate = usersServiceImpl.addUser(getUserFormTest());
        UsersForm usersForm = getUserFormTest();
        usersForm.setFirstName("name");
        User userUpdate = usersServiceImpl.update(userCreate.getId(), usersForm);
        assertNotEquals(userUpdate.getFirstName(), "test1");
        assertEquals(userCreate.getId(), userUpdate.getId());
    }

    @Test
    @Transactional
    void getReviewsByUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        Review review = reviewsServiceImpl.addReview(getReviewFormTest());
        usersServiceImpl.addReviewToUser(user.getId(), review.getId());
        List<Review> reviewsListTest = usersServiceImpl.getReviewsByUser(user.getId());
        assertEquals(reviewsListTest.get(0), review);
        assertEquals(reviewsListTest.get(0).getOwner(), user);
    }

    @Test
    @Transactional
    void getReviewsWithoutOwner() {
        Review review = reviewsServiceImpl.addReview(getReviewFormTest());
        List<Review> reviewsListTest = usersServiceImpl.getReviewsWithoutOwner();
        assertEquals(reviewsListTest.get(0), review);
        assertNull(reviewsListTest.get(0).getOwner());
    }

    @Test
    @Transactional
    void addReviewToUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        Review review = reviewsServiceImpl.addReview(getReviewFormTest());
        usersServiceImpl.addReviewToUser(user.getId(), review.getId());
        assertEquals(user.getId(), review.getOwner().getId());
    }

    @Test
    @Transactional
    void getBooksByUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        Book book = booksServiceImpl.addBook(getBookFormTest());
        usersServiceImpl.addBookToUser(user.getId(), book.getId());
        List<Book> booksListTest = usersServiceImpl.getBooksByUser(user.getId());
        assertEquals(booksListTest.get(0), book);
        assertEquals(booksListTest.get(0).getOwner(), user);
    }

    @Test
    @Transactional
    void getBooksWithoutOwner() {
        Book book = booksServiceImpl.addBook(getBookFormTest());
        List<Book> booksListTest = usersServiceImpl.getBooksWithoutOwner();
        assertEquals(booksListTest.get(0), book);
        assertNull(booksListTest.get(0).getOwner());
    }

    @Test
    @Transactional
    void addBookToUser() {
        User user = usersServiceImpl.addUser(getUserFormTest());
        Book book = booksServiceImpl.addBook(getBookFormTest());
        usersServiceImpl.addBookToUser(user.getId(), book.getId());
        assertEquals(user.getId(), book.getOwner().getId());
    }
}